from django.db import models


class Dealership(models.Model):
    name = models.CharField(max_length=255)
    address = models.CharField(max_length=255)
    phone = models.CharField(max_length=255)


class User(models.Model):
    phone = models.CharField(max_length=255)
    first_name = models.CharField(max_length=255, blank=True)
    last_name = models.CharField(max_length=255, blank=True)
    identity_number = models.CharField(max_length=255, blank=True)
    dealership = models.ForeignKey(Dealership, on_delete=models.CASCADE, related_name='users')

class Staff(models.Model):
